﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAPI.Interfaces;
using WebAPI.Models;

namespace WebAPI.Services
{
    public class DataManager : IDataManager
    {
        private IEnumerable<User> users = new List<User>
            {
                new User {Id = 0, Name = "Mary"},
                new User {Id = 1, Name = "Andriy"},
                new User {Id = 2, Name = "Ann"}
            };

        public IEnumerable<User> Users { get
            {
                return users;
            }

            set
            {
                users = value;
            }
        }

        public IEnumerable<User> GetUsers()
        {
            return users;
        }
    }
}