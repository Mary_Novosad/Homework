﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPI.Interfaces;
using WebAPI.Models;

namespace WebAPI.Controllers
{
    public class UserController : ApiController
    {
        private readonly IDataManager _dataManager;
        public UserController(IDataManager dataManager)
        {
            _dataManager = dataManager;
        }

        // GET: api/Users
        public IEnumerable<User> Get()
        {
            return _dataManager.GetUsers();
        }

        // GET: api/Users/5
        public IHttpActionResult Get(int id)
        {
            if (id < 0 || id > _dataManager.GetUsers().Count())
            {
                return BadRequest();
            }
            return Json(_dataManager.GetUsers().FirstOrDefault(x => x.Id == id));
        }

        // POST: api/Users
        public HttpResponseMessage Post(int id, [FromBody]string name)
        {
            if (id < _dataManager.GetUsers().Count() || String.IsNullOrWhiteSpace(name))
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }

            if (_dataManager.GetUsers().FirstOrDefault(x => x.Id == id) != null)
            {
                var response = new HttpResponseMessage
                {
                    Content = new StringContent("There is user with such Id")
                };
                return response;
            }

            _dataManager.GetUsers().ToList().Add(new User
            {
                Id = id,
                Name = name
            });

            return new HttpResponseMessage(HttpStatusCode.Created);
        }

        // PUT: api/Users/5
        public IHttpActionResult Put([FromBody]User user)
        {
            if (user.Id < 0 || user.Id >= _dataManager.GetUsers().Count())
            {
                return BadRequest();
            }
            var list = _dataManager.GetUsers().FirstOrDefault(x => x.Id == user.Id);
            if (list == null)
            {
                return InternalServerError();
            }
            list.Name = user.Name;
            return Ok();
        }

        // DELETE: api/Users/5
        public IHttpActionResult Delete(int id)
        {
            var user = _dataManager.GetUsers().FirstOrDefault(x => x.Id == id);
            if (user == null)
            {
                return BadRequest();
            }

            _dataManager.GetUsers().ToList().Remove(user);
            return Ok();
        }
      
    }
}
