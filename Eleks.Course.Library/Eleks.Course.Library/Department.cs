﻿using System;

namespace Eleks.Course.Library
{
    internal class Department
    {
        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public Department()
        {
            name = "";
        }
        public Department(string name)
        {
            this.name = name;
        }
    }
}
