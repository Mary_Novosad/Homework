﻿using System;
using System.Collections.Generic;

namespace Eleks.Course.Library
{
    internal class Book: IComparable, ICountingBooks
    {
        private string name;
        private int numberOfPages;
        private List<Author> authors;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public int NumberOfPages
        {
            get
            {
                return numberOfPages;
            }
            set
            {
                numberOfPages = value;
            }
        }
        public List<Author> Authors 
        {
            get
            {
                return authors;
            }
            set
            {
                authors = value;
            }
        }

        public Book()
        {
            authors = new List<Author>();
        }

        public Book(string name, List<Author> authors, int numberOfPages): this()
        {
            this.name = name;
            this.authors = authors;
            this.numberOfPages = numberOfPages;
        }
        public int CompareTo(object obj)
        {
            var item = obj as Book;
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            else
            {
                return numberOfPages.CompareTo(item.numberOfPages);
            }           
        }

        public int CountBooks()
        {
            return numberOfPages;
        }

        public override string ToString()
        {
            string result = "Name: " + name + " Number of pages: " + numberOfPages + " Authors: ";
            for(int i = 0; i < authors.Count; i++)
            {
                result += authors[i].FullName + " ";
            }
            return result;
        }
    }
}
