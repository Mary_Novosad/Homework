﻿using System;
using System.Collections.Generic;

namespace Eleks.Course.Library
{
    internal class LibraryDepartment: Department, IComparable, ICountingBooks
    {
        private List<Book> books;
        public List<Book> Books 
        {
            get
            {
                return books;
            }
            set
            {
                books = value;
            }
        }

        public Book this[int index]
        {
            get 
            { 
                return books[index];
            }
            set
            {  
                books[index] = value;
            }
        }

        public LibraryDepartment(): base()
        {
            books = new List<Book>();
        }

        public LibraryDepartment(string name, List<Book> books): base(name)
        {
            this.books = books;
        }
        public int CompareTo(object obj)
        {
            var item = obj as LibraryDepartment;
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            else
            {
                return books.Count.CompareTo(item.books.Count);
            }           
        }

        public int CountBooks()
        {
            return books.Count;
        }

        public override string ToString()
        {
            string result = "Name: " + Name + " Books: ";
            for (int i = 0; i < books.Count; i++)
            {
                result += books[i].Name;
            }
            return result;
        }
    }
}
