﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eleks.Course.Library
{
    class Author: IComparable, ICountingBooks
    {
        private string fullName;
        private int amountOfBooks;
        public string FullName
        {
            get
            {
                return fullName;
            }
            set
            {
                fullName = value;
            }
        }
        public int AmountOfBooks 
        {
            get
            {
                return amountOfBooks;
            }
            set
            {
                amountOfBooks = value;
            }
        }

        public Author(): base()
        {

        }

        public Author(string fullName, int amountOfBooks)
            : this()
        {
            this.fullName = fullName;
            this.amountOfBooks = amountOfBooks;
        }
        public int CompareTo(object obj)
        {
            var item = obj as Author;
            if (item == null)
            {
                throw new ArgumentNullException();
            }
            else
            {
                return amountOfBooks.CompareTo(item.amountOfBooks);
            }           
        }

        public int CountBooks()
        {
            return amountOfBooks;
        }

        public override string ToString()
        {
            string result = "Name: " + fullName + " Amount of books: " + amountOfBooks;
            return result;
        }
    }
}
