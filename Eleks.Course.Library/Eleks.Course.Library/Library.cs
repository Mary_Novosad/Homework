﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;

namespace Eleks.Course.Library
{
    internal class Library: IComparable, ICountingBooks
    {
        private List<LibraryDepartment> departments;
        public List<LibraryDepartment> Departments
        {
            get
            {
                return departments;
            }
            set
            {
                departments = value;
            }
        }

        public LibraryDepartment this[int index]
        {
            get
            {
                return departments[index];
            }
            set
            {
                departments[index] = value;
            }
        }
        public Library(): base()
        {
            departments = new List<LibraryDepartment>();
        }

        public Library(List<LibraryDepartment> departments) : this()
        {
            this.departments = departments;
        }
        public int CompareTo(object obj)
        {
            var item = obj as Library;
            if(item == null)
            {
                throw new ArgumentNullException();
            }
            else
            {
                return departments.Count.CompareTo(item.departments.Count);
            }           
        }

        public int CountBooks()
        {
            int sum = 0;
            foreach (var item in departments)
            {
                sum += item.CountBooks();
            }
            return sum;
        }

        public override string ToString()
        {
            string result = "Library consists of such departments: ";
            for (int i = 0; i < departments.Count; i++)
            {
                result += departments[i].Name;
            }
            return result;
        }

        public void Serialize(string filePath)
        {
            string output = JsonConvert.SerializeObject(this);
            File.WriteAllText(filePath, output);
        }

        public Library Deserialize(string filePath)
        {
            string output = File.ReadAllText(filePath);
            Library deserializedLibrary = JsonConvert.DeserializeObject<Library>(output);
            return deserializedLibrary;
        }
    }
}
