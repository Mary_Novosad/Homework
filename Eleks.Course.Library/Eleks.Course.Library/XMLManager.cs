﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace Eleks.Course.Library
{
    internal class LibraryXMLManager
    {
        public Library ReadFromXML(string fileName)
        {
            XDocument doc = XDocument.Load(fileName);
            List<LibraryDepartment> departments = new List<LibraryDepartment>();
            foreach (XElement department in doc.Root.Elements())
            {
                string departmentName = department.FirstAttribute.Value;
                List<Book> books = new List<Book>();
                foreach (XElement book in department.Elements())
                {
                    string bookName = book.FirstAttribute.Value;
                    int numberOfPages = Int32.Parse(book.LastAttribute.Value);                       
                    List<Author> authors = new List<Author>();
                    foreach (XElement author in book.Elements())
                    {
                        string authorName = author.FirstAttribute.Value;
                        int authorAmount = Int32.Parse(author.LastAttribute.Value);
                        Author tempAuthor = new Author(authorName, authorAmount);
                        authors.Add(tempAuthor);
                    }
                    Book tempBook = new Book(bookName, authors, numberOfPages);
                    books.Add(tempBook);
                }
                LibraryDepartment tempDepartment = new LibraryDepartment(departmentName, books);
                departments.Add(tempDepartment);
            }
            Library library = new Library(departments);
            return library;
        }

        public void UpdateXML(string fileName, Author author, Author newAuthor)
        {
            XDocument doc = XDocument.Load(fileName);
            var elements = doc.Descendants("department").Descendants("book").Descendants("author").Where(a => a.Attribute("name").Value == author.FullName);
            XElement replaceTo = new XElement("author", new XAttribute("name", newAuthor.FullName),
                            new XAttribute("amountOfBooks", newAuthor.AmountOfBooks));

            foreach (var item in elements)
            {
                item.ReplaceAll(replaceTo);
            }

            Console.WriteLine();
            doc.Save(fileName);
        }

        public void WriteToXML(string fileName, Library library)
        {
            XDocument document = new XDocument(new XDeclaration("1.0", "UTF-8", null));
            XElement lib = new XElement("library");
            document.Add(lib);
            for (int i = 0; i < library.Departments.Count; i++)
            {
                XElement department = new XElement("department");
                XAttribute name = new XAttribute("name", library[i].Name);
                department.Add(name);
                for (int j = 0; j < library[i].Books.Count; j++)
                {
                    XElement book = new XElement("book", new XAttribute("name", library[i][j].Name),
                        new XAttribute("numberOfPages", library[i][j].NumberOfPages));
                    for (int k = 0; k < library[i][j].Authors.Count; k++)
                    {
                        XElement author = new XElement("author", new XAttribute("name", library[i][j].Authors[k].FullName),
                            new XAttribute("amountOfBooks", library[i][j].Authors[k].AmountOfBooks));
                        book.Add(author);
                    }
                    department.Add(book);
                }
                lib.Add(department);
            }

            document.Save(fileName);
        }

        public void DeleteFromXML(string fileName, string departmentName)
        {
            XDocument doc = XDocument.Load(fileName);
            var result = doc.Descendants("department");
            foreach (var item in result)
            {
               if( item.Attribute("name").Value == departmentName)
                {
                    item.Remove();
                    break;
                }
            }
            doc.Save(fileName);           
        }
    }
}
