﻿using System;
using System.Collections.Generic;
using System.Linq;


namespace Eleks.Course.Library
{
    class Program
    {
        static void Main(string[] args)
        {
            Author author1 = new Author("S.King", 20);
            Author author2 = new Author("J.Rowling", 10);
            Author author3 = new Author("J.London", 10);

            List<Author> authors = new List<Author>();
            authors.Add(author1);

            List<Author> authors2 = new List<Author>();
            authors.Add(author2);
            authors2.Add(author3);
            authors2.Add(author1);
            Book book1 = new Book("Name1", authors, 100);
            Book book2 = new Book("Name2", authors2, 200);
            Book book3 = new Book("Name3", authors2, 200);

            List<Book> books = new List<Book>();
            books.Add(book1);
            books.Add(book3);
            books.Add(book2);
            LibraryDepartment department = new LibraryDepartment("department1", books);

            List<LibraryDepartment> departments = new List<LibraryDepartment>();
            departments.Add(department);

            Library library = new Library(departments);

            Console.WriteLine(book1.ToString());
            Console.WriteLine(library.ToString());
            Console.WriteLine(department.ToString());

            //Sorting (LINQ to object)
            var sortedByNameBooks = books.OrderBy(x => x.Name);
            foreach (var item in sortedByNameBooks)
            {
                Console.WriteLine(item);
                Console.WriteLine();
            }

            //searching (LINQ to object)
            var result = library.Departments.Where(x => (x.Books.Where(y => y.Authors.Exists(z => z == author3))
            .ToList().Count != 0))
            .Select(dep => new { Name = dep.Name });
            foreach (var item in result)
            {
                Console.WriteLine(item.Name);
            }

            //Writing to XML
            LibraryXMLManager manager = new LibraryXMLManager();

            manager.WriteToXML("Library.xml", library);
            //Reading from XML
            Library newLibrary = manager.ReadFromXML("Library.xml");
            
            //Updating XML (substitute all author1 on author3)
            manager.UpdateXML("Library.xml", author1, author3);

            //Deleting from XML department with name "department1"
            manager.DeleteFromXML("Library.xml", "department1");

            //Serialize library 
            library.Serialize("Library.json");

            //Deserialize library 
            Library deserializedLibrary = library.Deserialize("Library.json");
            Console.WriteLine("Deserialized library: {0}", library );
            

        }
    }
}
