﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eleks.Course.Library
{
    interface ICountingBooks
    {
        int CountBooks();
    }
}
