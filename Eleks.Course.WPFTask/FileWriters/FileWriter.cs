﻿using Microsoft.Practices.Unity;
using System.IO;

namespace Eleks.Course.WPFTask
{
    public class FileWriter : IFileWriter
    {
        private string filePath;
        private User user;
        private IUserSerializer serializer;
        private StreamWriter writer;
        public FileWriter()
        {

        }
        public FileWriter(string filePath, User user, IUserSerializer serializer)
        {
            this.filePath = filePath;
            this.user = user;
            this.serializer = serializer;
            writer = new StreamWriter(File.Open(filePath, FileMode.Append));
        }
        public void WriteToFile()
        {
            writer.WriteLine(serializer.Serialize(user));
            writer.Close(); 
        }
    }
}
