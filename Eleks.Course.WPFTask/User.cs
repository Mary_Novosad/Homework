﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Eleks.Course.WPFTask
{
    public class User
    { 
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public int Age { get; set; }
        public string PlaceOfWork { get; set; }

        public User()
        {

        }

        //public event PropertyChangedEventHandler PropertyChanged;
        //private void OnPropertyChanged(string propertyName)
        //{
        //    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        //}
    }
}
