﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eleks.Course.WPFTask
{
    public sealed class Container
    {
        private static readonly Lazy<Container> lazy = new Lazy<Container>(() => new Container());
        private static IUnityContainer unityContainer;
        private Container()
        {

        }
        static Container()
        {
            unityContainer = new UnityContainer();
        }

        public static T Resolve<T>()
        {
            T result = unityContainer.Resolve<T>();
            return result;
        }

        public static void RegisterType<T, U>() where U: T
        {
            unityContainer.RegisterType<T, U>();
        }

    }
}
