﻿using Eleks.Course.WPFTask.Commands;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Eleks.Course.WPFTask.ViewModels
{
    public class MainPageViewModel:INotifyPropertyChanged
    {
        private IFileWriter writer;
        private IHistory history;
        private IUserSerializer serializer;
        private string historyMessage;
        private User currentUser;
        public User CurrentUser
        {
            get
            {
                return currentUser;
            }
            set
            {
                currentUser = value;
            }
        }
        public string Root { get; set; }

        private ICommand saveCommand;
        private ICommand showHistoryCommand;

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public ICommand SaveCommand
        {
            get
            {
                return saveCommand;
            }
            set
            {
                OnPropertyChanged("SaveCommand");
                saveCommand = value;
            }
        }

        public ICommand ShowHistoryCommand
        {
            get
            {
                return showHistoryCommand;
            }
            set
            {
                OnPropertyChanged("ShowHistoryCommand");
                showHistoryCommand = value;
            }
        }

        public string HistoryMessage
        {
            get
            {
                return historyMessage;
            }
            set
            {
                OnPropertyChanged("HistoryMessage");
                historyMessage = value;
            }
        }

        public MainPageViewModel(IHistory history, IUserSerializer serializer)
        {
            SaveCommand = new Command(new Action<object>(Save));
            ShowHistoryCommand = new Command(new Action<object>(ShowHistory));
            CurrentUser = new User();
            CurrentUser.FirstName = "a";
            HistoryMessage = "";
            this.history = history;
            this.serializer = serializer;
        }

        public void ShowHistory(object obj)
        {
            List<string> messages = history.GetAllHistoryMessages().ToList<string>();
            StringBuilder temp = new StringBuilder();
            foreach (var item in messages)
            {
                temp.AppendLine(item);
            }
            HistoryMessage = temp.ToString();
        }

        public void Save(object obj)
        {
            history.AddUser(CurrentUser);
            writer = new FileWriter(Root, CurrentUser, serializer);
            writer.WriteToFile();
        }
    }
}
