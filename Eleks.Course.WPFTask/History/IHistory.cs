﻿using System.Collections.Generic;

namespace Eleks.Course.WPFTask
{
    public interface IHistory
    {
        void AddUser(User user);
        IEnumerable<string> GetAllHistoryMessages();
    }
}
