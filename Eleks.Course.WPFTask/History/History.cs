﻿using System.Collections.Generic;

namespace Eleks.Course.WPFTask
{
    class History : IHistory
    {
        private List<User> users;  
        public History()
        {
            users = new List<User>();
        }
        public void AddUser(User user)
        {
            users.Add(user);
        }

        public IEnumerable<string> GetAllHistoryMessages()
        {
            foreach (var item in users)
            {
                yield return item.FirstName + " " + item.SecondName + " registered.";
            }
        }
    }
}
