﻿
namespace Eleks.Course.WPFTask
{
    public class CSVUserSerializer : IUserSerializer
    {
        public CSVUserSerializer()
        {

        }
        public string Serialize(User user)
        {
            return user.FirstName + "," + user.SecondName + "," + user.Age.ToString() + "," + user.PlaceOfWork;
        }
    }
}
