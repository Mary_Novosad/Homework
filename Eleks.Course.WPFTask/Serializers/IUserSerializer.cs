﻿
namespace Eleks.Course.WPFTask
{
    public interface IUserSerializer
    {
        string Serialize(User user);
    }
}
