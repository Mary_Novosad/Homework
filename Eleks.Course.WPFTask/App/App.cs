﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Eleks.Course.WPFTask
{
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            Container.RegisterType<IHistory, History>();
            Container.RegisterType<IUserSerializer, CSVUserSerializer>();
            Container.RegisterType<IFileWriter, FileWriter>();
            var mainWindow = Container.Resolve<MainWindow>(); // Creating Main window
            mainWindow.Show();
        }

    }
}
