﻿using Eleks.Course.WPFTask.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Eleks.Course.WPFTask
{
    public partial class MainWindow : Window
    {
        public MainWindow(IHistory history, IUserSerializer serializer)
        {
            InitializeComponent();
            DataContext = new MainPageViewModel(history, serializer);
        }

    }
}

