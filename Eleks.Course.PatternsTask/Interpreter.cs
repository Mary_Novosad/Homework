using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eleks.Course.PatternsTask
{
    class Interpreter
    {
        private Command getCommand(string command, string details)
        {
            Command result;
            switch (command)
            {
                case "add":
                    result = new AddCommand(details);
                    break;
                case "get":
                    result =  new GetCommand(details);
                    break;
                case "delete":
                    result =  new DeleteCommand(details);
                    break;
                case "update":
                    result=  new UpdateCommand(details);
                    break;
                case "set":
                    result = new SetStorageCommand(details);
                    break;
                default:
                    throw new InvalidOperationException();
            }
            return result; 
        }
       public Command Interpret(string input)
        {
            int index = input.IndexOf(' ');
            string command = input.Substring(0, index);
            string details = input.Substring(index + 1);
            return getCommand(command, details);
        }
    }
}                                                                                                                                                                                                                                                                                 