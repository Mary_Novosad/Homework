﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eleks.Course.PatternsTask
{
    class Program
    {
        static void Main(string[] args)
        {
            Interpreter interpreter = new Interpreter();
            interpreter.Interpret("set storage Xml").Execute();
            interpreter.Interpret("add User id:12 firstname:mary lastname:novosad").Execute();
            interpreter.Interpret("add User id:123 firstname:mary lastname:novosad").Execute();
            interpreter.Interpret("add User id:122 firstname:mary lastname:novosad").Execute();
            interpreter.Interpret("delete User id:12").Execute();
            interpreter.Interpret("update User id:123 firstname:MARY lastname:NOVOSAD").Execute();

            //interpreter.Interpret("set storage Sql").Execute();
            //interpreter.Interpret("add User id:100 firstname:mary lastname:novosad").Execute();
            //interpreter.Interpret("add User id:1000 firstname:mary lastname:novosad").Execute();
            //interpreter.Interpret("update User id:100 firstname:MARY").Execute();
            //interpreter.Interpret("delete User id:1000").Execute();
            //interpreter.Interpret("delete User id:100").Execute();
        }
    }
}
