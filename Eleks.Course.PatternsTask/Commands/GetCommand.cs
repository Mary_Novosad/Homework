﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eleks.Course.PatternsTask
{
    class GetCommand: Command
    {
        private string id;
        public User User { get; private set; }
        public GetCommand(string details)
        {
            int index = details.IndexOf(' ');
            string type = details.Substring(0, index);
            details = details.Remove(0, index + 1);
            id = details.Substring(details.IndexOf(':') + 1);
        }
        public override void Execute()
        {
            User = Storage.Current.Get(id);
        }
    }
}
