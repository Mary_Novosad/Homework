using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;

namespace Eleks.Course.PatternsTask
{
    class AddCommand: Command
    {
        private User obj;
        public AddCommand(string details)
        {           
            int index = details.IndexOf(' ');
            string type = details.Substring(0, index);
            details = details.Remove(0, index + 1);
            var arguments = details.Split(' ');
            obj = Activator.CreateInstance<User>();
            foreach (var item in arguments)
            {
                var argument = item.Split(':');             
                PropertyInfo property = typeof(User).GetProperties().Where(x => x.Name.ToLower() == argument[0]).FirstOrDefault();                                             
                property.SetValue(obj, argument[1]);              
            }
        }

        public override void Execute()
        {
            Storage.Current.Add(obj);        
        }
    }

}                                                                                                                                                                                                                                                                                                                                          