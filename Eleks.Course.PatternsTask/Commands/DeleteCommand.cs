using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eleks.Course.PatternsTask
{
    class DeleteCommand: Command
    {
        private string id;
        public DeleteCommand(string details)
        {
            int index = details.IndexOf(' ');
            string type = details.Substring(0, index);
            details = details.Remove(0, index + 1);
            id = details.Substring(details.IndexOf(':') + 1);
        }
        public override void Execute()
        {
            Storage.Current.Delete(id);
        }
    }
}                                                                                                                                                                                                                                                            