﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Eleks.Course.PatternsTask
{
    class UpdateCommand: Command
    {
        private User user;
        public UpdateCommand(string details)
        {
            int firstIndex = details.IndexOf(' ');
            string type = details.Substring(0, firstIndex);
            details = details.Remove(0, firstIndex + 1);

            int secondIndex = details.IndexOf(' ');
            string id = details.Substring(0, secondIndex).Split(':')[1];
            details = details.Remove(0, secondIndex + 1);

            var arguments = details.Split(' ');
            user = Activator.CreateInstance<User>();
            user = new User(Storage.Current.Get(id));
            //user = new User(Storage.Current.Users.Find(user => user.Id == id));

            foreach (var item in arguments)
            {
                var argument = item.Split(':');
                var property = typeof(User).GetProperties().Where(x => x.Name.ToLower() == argument[0]).FirstOrDefault();
                if(property != default(PropertyInfo))
                {
                    property.SetValue(user, argument[1]);
                }
            }
        }
        public override void Execute()
        {
           Storage.Current.Update(user.Id, user);
        }
    }
}
