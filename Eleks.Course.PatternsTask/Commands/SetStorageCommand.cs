using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eleks.Course.PatternsTask
{
    class SetStorageCommand: Command
    {
        private StorageType storageType;
        public SetStorageCommand(string details)
        {
            int index = details.IndexOf(' ');
            string type = details.Substring(0, index);
            details = details.Remove(0, index + 1);
            storageType = (StorageType)Enum.Parse(typeof(StorageType), details);          
        }
        public override void Execute()
        {
            Storage.SetStorage(storageType);
        }
    }
}                                                                                                                                                                                                                                                                                                                                                    