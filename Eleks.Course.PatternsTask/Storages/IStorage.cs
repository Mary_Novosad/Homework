﻿using System.Collections.Generic;
namespace Eleks.Course.PatternsTask
{
    public interface IStorage
    {
        void Add(User obj);
        void Delete(string id);
        User Get(string id);
        void Update(string id, User newUser);
    }
}