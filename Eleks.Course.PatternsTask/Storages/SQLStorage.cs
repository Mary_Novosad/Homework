﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Eleks.Course.PatternsTask
{
    class SQLStorage : IStorage
    {
        private UserContext db;
        public SQLStorage()
        {            
            db = new UserContext();
        }

        public void Add(User obj)
        {
           db.Users.Add(obj);
           db.SaveChanges();
        }

        public void Delete(string id)
        {
            db.Users.Remove(Get(id));
            db.SaveChanges();
        }

        public User Get(string id)
        {
            return db.Users.Where(user => user.Id == id).FirstOrDefault();           
        }

        public void Update(string id, User newUser)
        {
            User user = Get(id);
            foreach (var property in typeof(User).GetProperties())
            {
                property.SetValue(user, property.GetValue(newUser));             
            }
            db.SaveChanges();
        }
    }
}
