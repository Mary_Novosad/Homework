﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Eleks.Course.PatternsTask
{
    class XMLStorage : IStorage
    {
        private StringWriter stringWriter;
        private XmlSerializer serializer;
        private List<User> users;
        private string filepath = "User.xml";
        public XMLStorage()
        {
            users = new List<User>();
            stringWriter = new StringWriter();
            serializer = new XmlSerializer(typeof(List<User>));
        }
        public void Add(User obj)
        {
            stringWriter = new StringWriter();
            users.Add(obj);           
            serializer.Serialize(stringWriter, users);
            File.WriteAllText(filepath, stringWriter.ToString());
        }

        public void Delete(string id)
        {
            stringWriter = new StringWriter();
            users.RemoveAt(users.FindIndex(x => x.Id == id ));
            serializer.Serialize(stringWriter, users);
            File.WriteAllText(filepath, stringWriter.ToString());
        }

        public void Update(string id, User newUser)
        {
            stringWriter = new StringWriter();
            int index = users.FindIndex(x => x.Id == id);
            users.RemoveAt(index);
            users.Insert(index, newUser);
            serializer.Serialize(stringWriter, users);
            File.WriteAllText(filepath, stringWriter.ToString());
        }

        public User Get(string id)
        {
            return users.First(x => x.Id == id);
        }

    }
}
