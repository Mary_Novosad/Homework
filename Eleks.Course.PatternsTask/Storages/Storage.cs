﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eleks.Course.PatternsTask
{
    class Storage
    {
        public static IStorage Current { get; private set; }
        public static void SetStorage(StorageType type)
        {
            switch (type)
            {
                case StorageType.Sql:
                    Current = new SQLStorage();
                    break;
                case StorageType.Xml:
                    Current = new XMLStorage();
                    break;
            }
        }
    }
}
