﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Contracts
{
    public class User
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
    }
}
