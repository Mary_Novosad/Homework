﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Contracts;

namespace WCF
{
    class Service : IContract
    {
        public string SendMessage(string message)
        {
            Console.WriteLine("Message: {0} is received", message);
            return "Thanks for using our service";
        }

        public string SendUser(User user)
        {
            Console.WriteLine("Hello {0} {1}!", user.Firstname, user.Lastname);
            return "User is sent";
        }
    }
}
