﻿using Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Client";
            Uri address = new Uri("http://localhost:5000/IContract");
            BasicHttpBinding binding = new BasicHttpBinding();
            EndpointAddress endpoint = new EndpointAddress(address);

            ChannelFactory<IContract> factory = new ChannelFactory<IContract>(binding, endpoint);
            IContract channel = factory.CreateChannel();

            Console.WriteLine("Type message");
            User user = new User { Firstname = "Mary", Lastname = "Novosad"};
            var answer = channel.SendUser(user);
            Console.WriteLine(answer + Environment.NewLine);
            var message = Console.ReadLine();
            while (message != "Close")
            {
                var response = channel.SendMessage(message);
                Console.WriteLine(response + Environment.NewLine);
                message = Console.ReadLine();

            }
        }
    }
}
