#pragma once
#include <vector>
#include <algorithm>
#include <fstream>
#include <string>
using namespace std;

class Contact
{
private:
	string name;
	string phoneNumber;
	string email;
public:
	string getName();
	Contact();
	Contact(string name, string phoneNumber, string email);
	friend istream& operator >> (istream& in, Contact& contact);
	friend ostream& operator << (ostream& out, Contact& contact);
};
