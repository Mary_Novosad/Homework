#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <string>
#include "Contact.h"
#include "Organizer.h"
using namespace std;

int main()
{
	ifstream input("Input.txt");
	ofstream output("Output.txt");
	Organizer organizer = Organizer();
	organizer.readFromFile(input);
	organizer.add(Contact("mary", "0987481312", "mn@gmail.com"));
	organizer.add(Contact("ann", "0987481312", "anna@gmail.com"));
	organizer.deleteByName("ann");
	cout << "User with name mary: " << organizer.findByName("mary");
	vector<Contact> contacts = organizer.getAllContacts();
	organizer.writeToFile(output);
	system("PAUSE");
	return 0;
}
