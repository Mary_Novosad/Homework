#pragma once
#include <vector>
#include <algorithm>
#include <fstream>
#include <string>
#include "Contact.h"
using namespace std;

class Organizer
{
private:
	vector<Contact> contacts;
public:
	Organizer();
	void add(Contact contact);
	Contact findByName(string name);
	void deleteByName(string name);
	vector<Contact> getAllContacts();
	void writeToFile(ostream& file);
	void readFromFile(istream& file);
};
