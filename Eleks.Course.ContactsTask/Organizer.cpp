#include "Organizer.h"

Organizer::Organizer() : contacts(vector<Contact>()) 
{

}
void Organizer::add(Contact contact)
{
	contacts.push_back(contact);
}
Contact Organizer::findByName(string name)
{
	auto result = find_if(contacts.begin(), contacts.end(), [&](Contact c) {return c.getName() == name; });
	return *result;
}
void Organizer::deleteByName(string name)
{
	contacts.erase(find_if(contacts.begin(), contacts.end(), [&](Contact c) {return c.getName() == name; }));
}
vector<Contact> Organizer::getAllContacts()
{
	return contacts;
}
void Organizer::writeToFile(ostream& file)
{
	for (int i = 0; i < contacts.size(); i++)
	{
		file << contacts[i];
	}
}
void Organizer::readFromFile(istream& file)
{
	int amount;
	file >> amount;
	for (int i = 0; i < amount; i++)
	{
		Contact temp;
		file >> temp;
		contacts.push_back(temp);
	}
}