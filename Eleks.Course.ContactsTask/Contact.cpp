#include "Contact.h"
using namespace std;

string Contact::getName()
{
	return name;
}
Contact::Contact()
{

}
Contact::Contact(string name, string phoneNumber, string email = "") : name(name), phoneNumber(phoneNumber), email(email) 
{

}
istream& operator >> (istream& in, Contact& contact)
{
	in >> contact.name >> contact.phoneNumber >> contact.email;
	return in;
}
ostream& operator << (ostream& out, Contact& contact)
{
	out << contact.name << " " << contact.phoneNumber << " " << contact.email << endl;
	return out;
}