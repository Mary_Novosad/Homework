﻿using Eleks.Course.DatabaseTask.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eleks.Course.DatabaseTask.Data
{
    public class UnitOfWork : IDisposable
    {
        private DbContext context = new Context();
        private Repository<Student> studentsRepository;
        private Repository<Group> groupsRepository;
        private Repository<Faculty> facultiesRepository;

        public Repository<Student> StudentsRepository
        {
            get
            {
                if (this.studentsRepository == null)
                {
                    this.studentsRepository = new Repository<Student>(context);
                }
                return studentsRepository;
            }
        }

        public Repository<Group> GroupsRepository
        {
            get
            {
                if (this.groupsRepository == null)
                {
                    this.groupsRepository = new Repository<Group>(context);
                }
                return groupsRepository;
            }
        }

        public Repository<Faculty> FacultiesRepository
        {
            get
            {
                if (this.facultiesRepository == null)
                {
                    this.facultiesRepository = new Repository<Faculty>(context);
                }
                return facultiesRepository;
            }
        }

        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
