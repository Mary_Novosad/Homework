﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eleks.Course.DatabaseTask.Data
{
    public class Repository<T> where T: class
    {
        private DbContext _context;
        private DbSet<T> _dbSet;
        public Repository()
        {
            _context = new Context();
            _dbSet = _context.Set<T>();
        }
        public Repository(DbContext context)
        {
            _context = context;
            _dbSet = _context.Set<T>();
        }

        public IEnumerable<T> Get( Func<T, bool> condition)
        {
            return _dbSet.Where(condition);
        }
        public T GetById(int id)
        {
            return _dbSet.Find(id);
        }
        public void Add(T entity)
        {
            _dbSet.Add(entity);
        }

        public void Delete(int id)
        {
            T entityToDelete = _dbSet.Find(id);
            _dbSet.Remove(entityToDelete);
        }

        public void Update(T newEntity)
        {
            _dbSet.Attach(newEntity);
            _context.Entry(newEntity).State = EntityState.Modified;
        }
    }
}
