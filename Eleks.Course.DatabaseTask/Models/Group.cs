﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Eleks.Course.DatabaseTask.Models
{
    public class Group
    {
        public int GroupId { get; set; }
        public virtual IEnumerable<Student> Students { get; set; }

    }
}
