﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eleks.Course.DatabaseTask.Models
{
    public class Faculty
    {
        public int FacultyId { get; set; }
        public virtual IEnumerable<Group> Groups { get; set; }
    }
}
