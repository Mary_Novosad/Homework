﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Eleks.Course.DatabaseTask.Models
{
    public class Student
    {
        public int StudentId { get; set; }
        public string Name { get; set; }
        public Group Group { get; set; }
        public Faculty Faculty { get; set; }

    }
}
