namespace Eleks.Course.DatabaseTask.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SmallChanges : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Students", "Faculty_FacultyId", "dbo.Faculties");
            DropForeignKey("dbo.Students", "Group_GroupId", "dbo.Groups");
            DropIndex("dbo.Students", new[] { "Faculty_FacultyId" });
            DropIndex("dbo.Students", new[] { "Group_GroupId" });
            DropPrimaryKey("dbo.Faculties");
            DropPrimaryKey("dbo.Groups");
            AlterColumn("dbo.Faculties", "FacultyId", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Groups", "GroupId", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Students", "Faculty_FacultyId", c => c.Int());
            AlterColumn("dbo.Students", "Group_GroupId", c => c.Int());
            AddPrimaryKey("dbo.Faculties", "FacultyId");
            AddPrimaryKey("dbo.Groups", "GroupId");
            CreateIndex("dbo.Students", "Faculty_FacultyId");
            CreateIndex("dbo.Students", "Group_GroupId");
            AddForeignKey("dbo.Students", "Faculty_FacultyId", "dbo.Faculties", "FacultyId");
            AddForeignKey("dbo.Students", "Group_GroupId", "dbo.Groups", "GroupId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Students", "Group_GroupId", "dbo.Groups");
            DropForeignKey("dbo.Students", "Faculty_FacultyId", "dbo.Faculties");
            DropIndex("dbo.Students", new[] { "Group_GroupId" });
            DropIndex("dbo.Students", new[] { "Faculty_FacultyId" });
            DropPrimaryKey("dbo.Groups");
            DropPrimaryKey("dbo.Faculties");
            AlterColumn("dbo.Students", "Group_GroupId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Students", "Faculty_FacultyId", c => c.String(maxLength: 128));
            AlterColumn("dbo.Groups", "GroupId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.Faculties", "FacultyId", c => c.String(nullable: false, maxLength: 128));
            AddPrimaryKey("dbo.Groups", "GroupId");
            AddPrimaryKey("dbo.Faculties", "FacultyId");
            CreateIndex("dbo.Students", "Group_GroupId");
            CreateIndex("dbo.Students", "Faculty_FacultyId");
            AddForeignKey("dbo.Students", "Group_GroupId", "dbo.Groups", "GroupId");
            AddForeignKey("dbo.Students", "Faculty_FacultyId", "dbo.Faculties", "FacultyId");
        }
    }
}
