namespace Eleks.Course.DatabaseTask.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Faculties",
                c => new
                    {
                        FacultyId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.FacultyId);
            
            CreateTable(
                "dbo.Groups",
                c => new
                    {
                        GroupId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.GroupId);
            
            CreateTable(
                "dbo.Students",
                c => new
                    {
                        StudentId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Faculty_FacultyId = c.String(maxLength: 128),
                        Group_GroupId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.StudentId)
                .ForeignKey("dbo.Faculties", t => t.Faculty_FacultyId)
                .ForeignKey("dbo.Groups", t => t.Group_GroupId)
                .Index(t => t.Faculty_FacultyId)
                .Index(t => t.Group_GroupId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Students", "Group_GroupId", "dbo.Groups");
            DropForeignKey("dbo.Students", "Faculty_FacultyId", "dbo.Faculties");
            DropIndex("dbo.Students", new[] { "Group_GroupId" });
            DropIndex("dbo.Students", new[] { "Faculty_FacultyId" });
            DropTable("dbo.Students");
            DropTable("dbo.Groups");
            DropTable("dbo.Faculties");
        }
    }
}
