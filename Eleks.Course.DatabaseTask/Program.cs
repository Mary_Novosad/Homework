﻿using Eleks.Course.DatabaseTask.Data;
using Eleks.Course.DatabaseTask.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eleks.Course.DatabaseTask
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var unitOfWork = new UnitOfWork())
            {
                var group1 = new Group();
                var group2 = new Group();
                var faculty1 = new Faculty();
                var faculty2 = new Faculty();
                unitOfWork.GroupsRepository.Add(group1);
                unitOfWork.GroupsRepository.Add(group2);
                unitOfWork.FacultiesRepository.Add(faculty1);
                unitOfWork.FacultiesRepository.Add(faculty2);
                var student = new Student
                {
                    Name = "Katy",
                    Group = unitOfWork.GroupsRepository.GetById(1),
                    Faculty = unitOfWork.FacultiesRepository.GetById(1)
                };
                unitOfWork.StudentsRepository.Delete(1);
                unitOfWork.StudentsRepository.Delete(2);
                unitOfWork.StudentsRepository.Delete(3);
                unitOfWork.StudentsRepository.Add(student);
                unitOfWork.Save();
                
                List<Student> result = unitOfWork.StudentsRepository.Get(st => st.Group.GroupId == 1).ToList();
                foreach(var st in result)
                {
                    Console.WriteLine(st.Name);
                }
                unitOfWork.Save();
            }
        }
    }
}
